<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * MenuItem
 *
 * @ORM\Table(name="menu_item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MenuItemRepository")
 */
class MenuItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url='';

    /**
     * @var Menu
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Menu", inversedBy="menuItems")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $menu;


    /**
     * @var MenuItem
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MenuItem", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $parent;

    /**
     * @var MenuItem[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MenuItem",mappedBy="parent", cascade={"persist"},orphanRemoval=true)
     */
    private $children;

    /**
     * @var MenuItemLanguage[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MenuItemLanguage",mappedBy="menuItem", cascade={"persist"},orphanRemoval=true)
     */
    private $menuItemLanguages;


    /**
     * MenuItem constructor.
     * @param MenuItemLanguage[] $menuItemLanguages
     * @param MenuItem[] $menuItems
     */
    public function __construct()
    {
        $this->menuItemLanguages = new ArrayCollection();
        $this->menuItems=new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return MenuItem
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set menuId
     *
     * @param integer $menu
     *
     * @return MenuItem
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menuId
     *
     * @return Menu
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @return MenuItem
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param MenuItem $parent
     * @return MenuItem
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return MenuItem[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param MenuItem[] $children
     * @return MenuItem
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return MenuItemLanguage[]
     */
    public function getMenuItemLanguages()
    {
        return $this->menuItemLanguages;
    }

    /**
     * @param MenuItemLanguage[] $menuItemLanguages
     * @return MenuItem
     */
    public function setMenuItemLanguages($menuItemLanguages)
    {
        $this->menuItemLanguages = $menuItemLanguages;
        return $this;
    }

    /**
     * @param int $id
     * @return MenuItem
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    public function addMenuItemLanguage(MenuItemLanguage $menuItemLanguage){
        $this->menuItemLanguages[]= $menuItemLanguage;
        $menuItemLanguage->setMenuItem($this);

        return $this;
    }

    public function removeMenuItemLanguage(MenuItemLanguage $menuItemLanguage){
        $this->menuItemLanguages->removeElement($menuItemLanguage);
    }

    function __toString()
    {
        return $this->url;
    }


}

