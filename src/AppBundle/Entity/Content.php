<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Content
 *
 * @ORM\Table(name="content")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContentRepository")
 */
class Content
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name='';

    /**
     * @var Galery
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Galery", mappedBy="content")
     */
    private $galery;

    /**
     * @var string
     *
     * @ORM\Column(name="url_short", type="string", length=255, nullable=true)
     */
    private $urlShort;


    /**
     * @var ContentLanguage[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ContentLanguage" ,mappedBy="content", cascade={"persist"}, orphanRemoval=true)
     */
    private $contentLanguages;

    /**
     * Get id
     *
     * @return int
     */
     public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Content
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set urlShort
     *
     * @param string $urlShort
     *
     * @return Content
     */
    public function setUrlShort($urlShort)
    {
        $this->urlShort = $urlShort;

        return $this;
    }

    /**
     * Get urlShort
     *
     * @return string
     */
    public function getUrlShort()
    {
        return $this->urlShort;
    }

    /**
     * @return ContentLanguage[]
     */
    public function getContentLanguages()
    {
        return $this->contentLanguages;
    }

    /**
     * @param ContentLanguage[] $contentLanguages
     * @return Content
     */
    public function setContentLanguages($contentLanguages)
    {
        $this->contentLanguages = $contentLanguages;
        return $this;
    }

    /**
     * @param int $id
     * @return Content
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    function __toString()
    {
        return $this->name;
    }

    /**
     * @return Galery
     */
    public function getGalery()
    {
        return $this->galery;
    }

    /**
     * @param Galery $galery
     * @return Content
     */
    public function setGalery($galery)
    {
        $this->galery = $galery;
        return $this;
    }




}
