<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MenuItemLanguage
 *
 * @ORM\Table(name="menu_item_language")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MenuItemLanguageRepository")
 */
class MenuItemLanguage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $language;

    /**
     * @var MenuItem
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MenuItem",inversedBy="menuItemLanguages")
     * @ORM\JoinColumn(name="menu_item_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $menuItem;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name='';


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param integer $language
     *
     * @return MenuItemLanguage
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set menuItem
     *
     * @param MenuItem $menuItem
     *
     * @return MenuItemLanguage
     */
    public function setMenuItem($menuItem)
    {
        $this->menuItem = $menuItem;

        return $this;
    }

    /**
     * Get menuItemId
     *
     * @return MenuItem
     */
    public function getMenuItem()
    {
        return $this->menuItem;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MenuItemLanguage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param int $id
     * @return MenuItemLanguage
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    function __toString()
    {
        return $this->name;
    }

}

