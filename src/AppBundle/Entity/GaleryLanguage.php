<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GaleryLanguage
 *
 * @ORM\Table(name="galery_language")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GaleryLanguageRepository")
 */
class GaleryLanguage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Language",inversedBy="galeryLanguages")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $language;

    /**
     * @var Galery
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Galery", inversedBy="galeryLanguages")
     * @ORM\JoinColumn(name="galery_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $galery;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param integer $language
     *
     * @return GaleryLanguage
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set galeryId
     *
     * @param Galery $galery
     *
     * @return GaleryLanguage
     */
    public function setGalery(Galery $galery)
    {
        $this->galery = $galery;

        return $this;
    }

    /**
     * Get galery
     *
     * @return Galery
     */
    public function getGalery()
    {
        return $this->galery;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GaleryLanguage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    function __toString()
    {
        return $this->name;
    }


}

