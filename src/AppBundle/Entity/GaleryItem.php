<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * GaleryItem
 *
 * @ORM\Table(name="galery_item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GaleryItemRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class GaleryItem extends AbstractType

{

    const SERVER_PATH_TO_IMAGE_FOLDER = 'C:\xampp\htdocs\site-prezentare-ap\web\uploads';

    /**
     * Unmapped property to handle file uploads
     */
    private $file;

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and target filename as params
        $this->getFile()->move(
            self::SERVER_PATH_TO_IMAGE_FOLDER,
            $this->getFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->image = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }

    /**
     * @ORM\PrePersist
     */
    public function lifecycleFileUpload()
    {
        $this->upload();
    }

    /**
     * Updates the hash value to force the preUpdate and postUpdate events to fire.
     */
    public function refreshUpdated()
    {
        $this->setUpdated(new \DateTime());
    }



    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Galery
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Galery", inversedBy="galeryItems")
     * @ORM\JoinColumn(name="galery_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $galery;



    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var \DateTime|null
     */
    private $updated;

    /**
     * @return \DateTime|null
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime|null $updated
     * @return GaleryItem
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }






    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return GaleryItem
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Galery
     */
    public function getGalery()
    {
        return $this->galery;
    }

    /**
     * @param Galery $galery
     * @return GaleryItem
     */
    public function setGalery($galery)
    {
        $this->galery = $galery;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return GaleryItem
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    function __toString()
    {
            return $this->image;
    }

    public function getFirstImage(){
        if ($this->image->first()) {
            return $this->image->first();
        } else {
            $noImage = new GaleryItem();
            $noImage->setImage('no_image.png');

            return $noImage;
        }
    }




}

