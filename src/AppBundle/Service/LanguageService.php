<?php
/**
 * Created by PhpStorm.
 * User: Mihaela
 * Date: 07/01/2020
 * Time: 13:39
 */

namespace AppBundle\Service;


use AppBundle\Entity\Language;
use Doctrine\Common\Persistence\ManagerRegistry;


class LanguageService
{
    /** @var  ManagerRegistry */
    private $entityManager;

    private $session;

    public function getLanguages(){
        return $this->getEntityManager()->getRepository(Language::class)->findAll();
    }


    public function getCurrentLanguage(){
        return $this->getEntityManager()->getRepository(Language::class)->findOneBy(['name'=>$this->getSession()->get('_locale','ro')]);

    }


    /**
     * @return ManagerRegistry
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param ManagerRegistry $entityManager
     * @return LanguageService
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param mixed $session
     * @return LanguageService
     */
    public function setSession($session)
    {
        $this->session = $session;
        return $this;
    }




}