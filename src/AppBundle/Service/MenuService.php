<?php
/**
 * Created by PhpStorm.
 * User: Irina
 * Date: 08.01.2020
 * Time: 10:47
 */

namespace AppBundle\Service;
use AppBundle\Entity\Content;
use AppBundle\Entity\Menu;
use AppBundle\Entity\MenuItem;
use AppBundle\Entity\MenuItemLanguage;
use Doctrine\Common\Persistence\ManagerRegistry;


class MenuService
{
    /** @var  ManagerRegistry */
    private $entityManager;
    private $session;




    public function getUrl()
    {
        return $this->getEntityManager()->getRepository(MenuItem::class)->findAll();
    }



    public function getRootMenuItems($name){
        $list=[];
        $menu = $this->getEntityManager()->getRepository(Menu::class)->findOneBy(['name'=>$name]);

        foreach($menu->getMenuItems() as $menuItem)
        {
            if(is_null($menuItem->getParent())){
                $list[]=$menuItem;
            }
        }

        return $list;
    }

    public function getContent()
    {

        $cl = $this->getEntityManager()->getRepository(Content::class)->findAll();
        return $cl;

    }

    /**
     * @return ManagerRegistry
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param ManagerRegistry $entityManager
     * @return MenuService
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }
    /**
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param mixed $session
     * @return MenuService
     */
    public function setSession($session)
    {
        $this->session = $session;
        return $this;
    }

}