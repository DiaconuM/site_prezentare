<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Content;
use AppBundle\Entity\ContentLanguage;
use AppBundle\Entity\Galery;
use AppBundle\Entity\GaleryItem;
use AppBundle\Entity\MenuItem;
use AppBundle\Entity\MenuItemLanguage;
use AppBundle\Entity\Slider;
use AppBundle\Entity\SliderItem;
use FOS\UserBundle\Controller\SecurityController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;


class DefaultController extends SecurityController
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        $items = $this->getDoctrine()->getManager();
        $sliderItems = $items->getRepository(SliderItem::class)->findAll();
        return $this->render('default/index.html.twig', ['sliderItems' => $sliderItems]);
    }


    /**
     * @Route("language/{name}", name="language")
     */
    public function languageAction(Request $request, $name = 'Ro')
    {


        $this->get('session')->set('_locale', $name);
        $request->setLocale($name);
        return $this->redirect($request->headers->get('referer'));


    }


    /**
     * @Route("menu/{url}", name="menu")
     */

    public function switchMenuAction(Request $request, $url = '', $language = 'en')
    {


        $this->get('session')->set('_locale', $url);
        $request->setLocale($url);
        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * @Route("content/{name}", name="content")
     */
    public function contentAction(Request $request, $name)
    {

        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository(Content::class)->findOneBy(['name' => $name]);


        foreach ($content->getContentLanguages() as $contentLanguage) {
            if ($this->get('session')->get('_locale', 'Ro') == $contentLanguage->getLanguage()->getName()) {
                $cl = $contentLanguage;
            }


        if (!isset($cl)) {
            $cl = $content->getContentLanguages()[0];
        }

        return $this->render('default/content.html.twig', ['contentLanguage' => $cl]);
    }
    }


    /**
     * @Route ("galery/{id}", name="galerySingle")
     */
    public function galerySingleAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $galery = $em->getRepository(galery::class)->find($id);
        $galeryItems = $em->getRepository(galeryItem::class)->findAll();

        $galeryLanguages = $galery->getContent()->getContentLanguages();

        foreach ($galeryLanguages as $galeryLanguage) {
            if ($this->get('session')->get('_locale', 'Ro') == $galeryLanguage->getLanguage()->getName()) {
                $gl = $galeryLanguage;
            }
        }

        if (!isset($gl)) {
            $gl = $galeryLanguage->getContentLanguages()[0];
        }

        return $this->render('default/galerySingle.html.twig', ['galery' => $galery, 'galeryItems' => $galeryItems, 'gl' => $gl]);
    }


    /**
     * @Route("galery", name="galery")
     */
    public function galeryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $galeries = $em->getRepository(galery::class)->findAll();

        return $this->render('default/galery.html.twig', ['galeries' => $galeries]);

    }


    /**
     * @Route("/login", name="fos_user_security_login")
     */
    public function UserLoggedAction(Request $request)
    {
        $tokenInterface = $this->get('security.token_storage')->getToken();

        if (!$tokenInterface instanceof AnonymousToken) {
            return $this->redirectToRoute('sonata_admin_dashboard');
        } else {
            return $this->loginAction($request);
        }

    }
}








